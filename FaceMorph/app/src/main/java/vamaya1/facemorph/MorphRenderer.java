package vamaya1.facemorph;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MorphRenderer implements GLSurfaceView.Renderer {

    private final Context activityContext;
    private final MorphSurfaceView morphSurfaceView;

    private final float[] projectionMatrix = new float[16]; // project scene onto 2d viewport
    private final float[] viewMatrix = new float[16]; // position scene relative to eye view
    private float[] mvpMatrix = new float[16]; // allocate storage for final combined matrix to be passed to shader program

    Rectangle rectangle;

    public MorphRenderer(final Context context, final MorphSurfaceView surfaceView) {

        activityContext = context;
        morphSurfaceView = surfaceView;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // set background frame color to black

        // position eye in front of origin
        final float eyeX = 0.0f;
        final float eyeY = 0.0f;
        final float eyeZ = -3.0f; //-0.5f;
        // facing the screen
        final float lookX = 0.0f;
        final float lookY = 0.0f;
        final float lookZ = 0.0f; //-5.0f;
        // viewer vector (direction of the head of person holding the camera)
        final float upX = 0.0f;
        final float upY = 1.0f;
        final float upZ = 0.0f;
        // set the camera position (view matrix)
        Matrix.setLookAtM(viewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

        rectangle = new Rectangle(activityContext, morphSurfaceView);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

        GLES20.glViewport(0, 0, width, height); // adjust viewport based on geometry changes (ex: screen rotation)

        // create new perspective projection matrix
        // height stays same while width will vary as per aspect ratio
        final float ratio = (float) width / height;
        final float left = -ratio;
        final float right = ratio;
        final float bottom = -1.0f;
        final float top = 1.0f;
        final float near = 3.0f; //1.0f; // 3.0f
        final float far = 7.0f; //10.0f; // 7.0f
        // projection matrix applied to object coordinates in DrawFrame()
        Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
    }

    @Override
    public void onDrawFrame(GL10 gl) {

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT); // draw background color

        // calculate projection and view transformation
        // matrix multiplication: mvpMatrix = projectionMatrix * viewMatrix
        Matrix.multiplyMM(mvpMatrix, 0, projectionMatrix, 0, viewMatrix, 0);

        rectangle.draw(mvpMatrix);
    }
}
