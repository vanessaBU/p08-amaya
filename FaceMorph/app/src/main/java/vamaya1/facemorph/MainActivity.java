package vamaya1.facemorph;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = (ImageButton)findViewById(R.id.startButton); // get start button
        startButton.setOnClickListener(this); // add click listener
    }

    @Override
    public void onClick(View v) {

        startActivity(new Intent(this, MorphActivity.class)); // start game activity
    }
}
