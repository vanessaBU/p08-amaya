package vamaya1.facemorph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ImageNames {

    Random randomGenerator; // seed for random generator

    public ImageNames() {
        randomGenerator = new Random(); // initialize seed for random generator
    }

    public int getSize() {
        return imageNames.length;
    }

    public String getName(int n) {
        return imageNames[n];
    }

    public int getRandomImageNumber() {
        return randomGenerator.nextInt(imageNames.length);
    }

    // get random image name excluding the numbers in the list provided
    public int getRandomImageNumber(List<Integer> exclusions) {

        Collections.sort(exclusions); // sort list

        if (exclusions.get(exclusions.size() - 1) < Integer.valueOf(imageNames.length - 1)) {
            exclusions.add(imageNames.length); // select random from tail range also
        }

        List<Integer> choices = new ArrayList<>(); // list to select random number from

        int minN = 0;
        for (Integer maxN : exclusions) {
            // ignore values out of bounds
            if (maxN < 0) {
                maxN = 0;
            }
            else if (maxN >= imageNames.length) {
                maxN = imageNames.length;
            }
            int rangeSize = maxN - minN; // excludes maxN in random
            if (rangeSize > 0) {
                Integer randomNumber = randomGenerator.nextInt(rangeSize) + minN;
                choices.add(randomNumber);
            }
            // set up next iteration
            minN = maxN + 1;
        }

        int randomChoice = randomGenerator.nextInt(choices.size()); // returns 0 to (size - 1)
        Integer finalRandom = choices.get(randomChoice); // get number from choices list

        return finalRandom;
    }

    // array of all possible image name choices
    private static final String[] imageNames = {
            "alpaca",
            "armadillo",
            "badger",
            "bear",
            "bee",
            "bunny",
            "butterfly",
            "camel",
            "cat",
            "caterpillar",
            "chameleon",
            "cheetah",
            "cougar",
            "crow",
            "deer",
            "dingo",
            "dog",
            "eagle",
            "elephant",
            "fish",
            "frog",
            "gecko",
            "giraffe",
            "goat",
            "horse",
            "iguana",
            "koala",
            "ladybug",
            "leopard",
            "lion",
            "owl",
            "panda",
            "parrot",
            "penguin",
            "porcupine",
            "seal",
            "sloth",
            "snake",
            "spider",
            "squirrel",
            "tiger",
            "toucan",
            "turtle",
            "wolf"
    };
}
