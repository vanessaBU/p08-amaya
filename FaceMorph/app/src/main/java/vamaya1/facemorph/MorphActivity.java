package vamaya1.facemorph;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MorphActivity extends Activity implements View.OnClickListener {

    private GLSurfaceView morphSurfaceView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        morphSurfaceView = new MorphSurfaceView(this); // create GLSurfaceView instance

        setContentView(morphSurfaceView); // set GLSurfaceView as ContentView for this activity
    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(this, GuessActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onPause() {

        super.onPause();
        morphSurfaceView.onPause();
    }

    @Override
    protected void onResume() {

        super.onResume();
        // re-allocate graphic objects de-allocated in onPause() here
        morphSurfaceView.onResume();
    }
}
