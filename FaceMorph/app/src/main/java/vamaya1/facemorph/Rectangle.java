package vamaya1.facemorph;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLES30;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public class Rectangle {

    private final Context activityContext;
    private final MorphSurfaceView morphSurfaceView;

    // vertex information
    static final int VERTEX_DIMS = 3;
    static final int TEXTURE_DIMS = 2;
    static final int FLOAT_BYTES = 4;
    static final int SHORT_BYTES = 2;
    static float vertices[] = {
            -0.5625f, 1.0f, 0.0f,   // 0 top left
            -0.5625f, -1.0f, 0.0f,  // 1 bottom left
            0.5625f, -1.0f, 0.0f,   // 2 bottom right
            0.5625f, 1.0f, 0.0f };  // 3 top right
    static float textureVertices[] = {
            1.0f, 0.0f,     // 0 bottom right   // top left
            1.0f, 1.0f,     // 1 top right      // bottom left
            0.0f, 1.0f,     // 2 top left       // bottom right
            0.0f, 0.0f, };  // 3 bottom left    // top right
    private final short drawOrder[] = { 0, 1, 2, 0, 2, 3 }; // order to draw vertices
    private final int vertexStride = VERTEX_DIMS * FLOAT_BYTES; // bytes per vertex

    private final FloatBuffer vertexBuffer;
    private final ShortBuffer drawListBuffer;
    private final FloatBuffer textureBuffer;

    private final int shaderProgram;
    private int positionHandle;
    private int colorHandle;
    private int mvpMatrixHandle;
    private int textureUniformHandle;

    private int xMoveHandle;
    private int yMoveHandle;
    private int widthHandle;
    private int heightHandle;

    private int textureHandle;
    private int[] textureDataHandle = new int[1];

    private int imageID;

    float color[] = { 0.0f, 1.0f, 0.0f, 1.0f }; // green

    public Rectangle(Context context, final MorphSurfaceView surfaceView) {

        activityContext = context;
        morphSurfaceView = surfaceView;

        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                vertices.length * FLOAT_BYTES); // (# of coordinate values * 4 bytes per float)
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.position(0);

        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(
                drawOrder.length * SHORT_BYTES); // (# of coordinate values * 2 bytes per short)
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        // initialize texture buffer
        ByteBuffer tbb = ByteBuffer.allocateDirect(
                textureVertices.length * FLOAT_BYTES); // (# of coordinate values * 4 bytes per float)
        tbb.order(ByteOrder.nativeOrder());
        textureBuffer = tbb.asFloatBuffer();
        textureBuffer.put(textureVertices);
        textureBuffer.position(0);

        // prepare shader and OpenGL program
        String vertexShaderCode = getShader(R.raw.rectangle_vertex_shader);
        String fragmentShaderCode = getShader(R.raw.rectangle_fragment_shader);
        final int vertexShaderHandle = MyGLRenderer.loadShader(
                GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        final int fragmentShaderHandle = MyGLRenderer.loadShader(
                GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);
        shaderProgram = GLES20.glCreateProgram(); // create empty OpenGL Program
        GLES20.glAttachShader(shaderProgram, vertexShaderHandle); // add the vertex shader to program
        GLES20.glAttachShader(shaderProgram, fragmentShaderHandle); // add the fragment shader to program
        GLES20.glLinkProgram(shaderProgram); // create OpenGL program executables

        imageID = morphSurfaceView.getImageID();
        textureDataHandle[0] = TextureHelper.loadTexture(activityContext, imageID);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureDataHandle[0]);
    }

    public void draw(float[] mvpMatrix) {

        // bind texture if image changed
        int resID = morphSurfaceView.getImageID();
        if (resID != imageID) {
            GLES20.glDeleteTextures(1, textureDataHandle, 0); // delete old texture
            textureDataHandle[0] = TextureHelper.loadTexture(activityContext, resID); // set new texture
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureDataHandle[0]); // bind to current
            imageID = resID; // update image id
        }

        GLES20.glUseProgram(shaderProgram); // add shader program to OpenGL environment

        positionHandle = GLES20.glGetAttribLocation(shaderProgram, "aPosition"); // get handle to vertex shader position
        GLES20.glEnableVertexAttribArray(positionHandle); // enable handle to vertices
        // prepare triangle coordinate data
        GLES20.glVertexAttribPointer(positionHandle, VERTEX_DIMS, GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);

        colorHandle = GLES20.glGetUniformLocation(shaderProgram, "uColor"); // get handle to fragment shader color
        GLES20.glUniform4fv(colorHandle, 1, color, 0); // set color for drawing

        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgram, "uMVPMatrix"); // get handle to shape transformation matrix
        MyGLRenderer.checkGlError("glGetUniformLocation");

        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, mvpMatrix, 0); // apply projection and view transformation
        MyGLRenderer.checkGlError("glUniformMatrix4fv");

        textureUniformHandle = GLES20.glGetUniformLocation(shaderProgram, "uTexture");

        xMoveHandle = GLES20.glGetUniformLocation(shaderProgram, "uXMove");
        MyGLRenderer.checkGlError("glGetUniformLocation");
        yMoveHandle = GLES20.glGetUniformLocation(shaderProgram, "uYMove");
        MyGLRenderer.checkGlError("glGetUniformLocation");
        float xMove = morphSurfaceView.moving ? morphSurfaceView.moveX : -1.0f;
        float yMove = morphSurfaceView.moving ? morphSurfaceView.moveY : -1.0f;
        GLES20.glUniform1f(xMoveHandle, xMove);
        GLES20.glUniform1f(yMoveHandle, yMove);

        int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        widthHandle = GLES20.glGetUniformLocation(shaderProgram, "uWidth");
        MyGLRenderer.checkGlError("glGetUniformLocation");
        GLES20.glUniform1f(widthHandle, (float)screenWidth);
        heightHandle = GLES20.glGetUniformLocation(shaderProgram, "uHeight");
        MyGLRenderer.checkGlError("glGetUniformLocation");
        GLES20.glUniform1f(heightHandle, (float)screenHeight);

        textureHandle = GLES20.glGetAttribLocation(shaderProgram, "aTextureCoordinate");
        GLES20.glVertexAttribPointer(textureHandle, TEXTURE_DIMS, GLES20.GL_FLOAT, false, 0, textureBuffer);
        GLES20.glEnableVertexAttribArray(textureHandle);

        GLES20.glUniform1i(textureUniformHandle, 0); // tell texture uniform sampler to use this texture in the shader by binding to texture unit 0

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, drawListBuffer); // draw rectangle

        GLES20.glDisableVertexAttribArray(positionHandle); // disable vertex array
    }

    private String getShader(final int resourceID) {

        return RawResourceReader.readTextFileFromRawResource(activityContext, resourceID);
    }
}
