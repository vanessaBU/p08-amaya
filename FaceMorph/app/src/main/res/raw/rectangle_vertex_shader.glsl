uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;

attribute vec4 aPosition;
attribute vec4 aColor;
attribute vec2 aTextureCoordinate;

varying vec3 vPosition;
varying vec4 vColor;
varying vec2 vTextureCoordinate;

void main()
{
    vPosition = vec3(uMVMatrix * aPosition);

    vColor = aColor;

    vTextureCoordinate = aTextureCoordinate;

    gl_Position = uMVPMatrix * aPosition;

    //gl_Position = uMVPMatrix * vec4(aPosition.x, aPosition.y, depth, 1.0);
}
