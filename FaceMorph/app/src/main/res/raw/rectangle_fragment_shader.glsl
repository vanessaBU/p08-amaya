precision mediump float;

uniform sampler2D uTexture;
uniform vec4 uColor;

uniform float uXMove;
uniform float uYMove;
uniform float uHeight;
uniform float uWidth;

varying vec2 vTextureCoordinate;
varying vec3 vPosition;
varying vec4 vColor;

void main()
{
    bool moving = uXMove != -1.0 && uYMove != -1.0;
    vec2 move = vec2(uXMove, uYMove);
    vec2 coord = vec2(gl_FragCoord);
    vec2 viewport = vec2(uWidth, uHeight);
    float maxlen = 100.0;

    float opacity = 0.10;
    if (moving) {
        move.y = viewport.y - move.y;
        float len = length(coord - move);
        if (len < maxlen) {
            opacity = 1.0 - len / maxlen;
        }
    }
    gl_FragColor = texture2D(uTexture, vTextureCoordinate);
    gl_FragColor *= opacity;
}
