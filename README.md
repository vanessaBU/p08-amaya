Shot in the Dark
-----------------

This is an Android game created in Android Studio with OpenGL. The game starts out with a dark screen. Use your finger to move around the screen and a small light pool will illuminate a piece of the image. Drag your finger around to view different sections in the light pool. The objective of the game is to guess the image correctly. Once you are ready, tap the screen and you will get options to choose from. After you guess, tap to return and play again with a different image.

I used some portions of the following tutorials to help bridge some of the learning gaps with OpenGL:
1. Shape renderer tutorial: https://developer.android.com/training/graphics/opengl/environment.html#renderer
2. Texture loading tutorial: http://www.learnopengles.com/android-lesson-four-introducing-basic-texturing/